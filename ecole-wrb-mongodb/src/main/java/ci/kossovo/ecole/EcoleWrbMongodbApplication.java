package ci.kossovo.ecole;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(EcoleMetierMongodbApplication.class)
public class EcoleWrbMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(EcoleWrbMongodbApplication.class, args);
	}
}
